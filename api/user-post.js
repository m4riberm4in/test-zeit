const express = require('express');
const helmet = require('helmet');
const moment = require('moment');
const connect = require('./connect');

const app = express();

app.use(helmet());
app.use(express.urlencoded());

const saveUser = async (username, phone) => {
	let result = null;
	const db = await connect(process.env.MONGODB_URI);
	const collection = await db.collection('users');
	result = await collection.insertOne({username, phone});	
	return result.ops;
}

app.post('/users', async (req, res) => {
  const { username, phone } = req.body;
  console.log(req.body);
  const user = await saveUser(username, phone);
  if(user)
  	res.status(200).json(user);
  else
  	res.status(400).send('something wrong');
});

module.exports = app;
