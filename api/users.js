const express = require('express');
const helmet = require('helmet');
const moment = require('moment');
const connect = require('./connect');

const app = express();

app.use(helmet());
app.use(express.urlencoded());

app.get('/users', async (req, res)  => {
	const db = await connect(process.env.MONGODB_URI);
	const collection = await db.collection('users')
	// Select the users collection from the database
	const users = await collection.find({}).toArray()
	res.status(200).json({users});
});

module.exports = app;
