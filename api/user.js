const express = require('express');
const helmet = require('helmet');
const moment = require('moment');

const app = express();
const mockUser = [{
	id:1,
	username:'username is here',
	email: 'email is here',
	phone:'0121212'
},
{
	id:2,
	username:'b',
	email: 'email is here',
	phone:'0121212'
},
]

app.use(helmet());
app.use(express.urlencoded());

app.get('/user/:id', (req, res) => {
	const { id } = req.params;
	if(id){
		const find = mockUser.find(user => user.id === parseInt(id));
		console.log(find);
		if(find){
			res.set('Content-Type', 'json');
			res.status(200).send(find);
		}
		else{
			res.set('Content-Type', 'json');
			res.status(200).send({status:404, message:'user not found'});	
		}
	}
	else{
		res.set('Content-Type', 'json');
		res.status(200).send(mockUser);
	}


});

module.exports = app;
